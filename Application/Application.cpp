// Application.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\Library\ClassName.h"

template <class number>
int getNum(number &r){
	std::cin >> r;
	if (!std::cin.good())
		return 1;
	return 0;
}

int create(Logic::BVector**);
int erase(Logic::BVector**);
int set(Logic::BVector**);
int getVal(Logic::BVector**);
int getN(Logic::BVector**);
int OR(Logic::BVector**);
int AND(Logic::BVector**);
int EQAND(Logic::BVector**);
int NOT(Logic::BVector**);
int DEF(Logic::BVector**);
int ISEQ(Logic::BVector**);
int input(Logic::BVector**);
int output(Logic::BVector**);
int cut(Logic::BVector**v);


void erasefull(Logic::BVector**);

int choice(int&, Logic::BVector**);

int(*f[])(Logic::BVector**) = { NULL, create, getN, getVal, OR, AND, EQAND, NOT, ISEQ, DEF, erase, set, input, output, cut };
const char*msgs[] = { "0. Quit", "1. Create 3 vectors","2. Get number", "3. Get value", "4. z = x | y", "5. z = x & y ", "6. x &= y", "7. x ^ y","8. x == y", "9. Check if defined", "10. Erase vector", "11. Set vector", "12. Input vector", "13. Output vector","14. Cut" };
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

int _tmain(int argc, _TCHAR* argv[])
{
	Logic::BVector *BVector[3] = { nullptr, nullptr, nullptr };
	int ch, err;
	do{
		err=choice(ch, BVector);
		if (err){
			erasefull(BVector);
			return 0;
		}
	} while (ch);
	erase(BVector);
	return 0;
}

int choice(int&ch, Logic::BVector** BVector){
	int err=0;
	std::cout << std::endl;
	for (int i = 0; (i < NMsgs) && (i < 2 || BVector[0] != nullptr); i++){
		std::cout << msgs[i] << std::endl;
	}
	do{
		if (getNum(ch)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (ch < 0||ch>=NMsgs||((ch>1)&&(BVector[0]==nullptr))){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (ch < 0 || ch >= NMsgs || ((ch>1) && (BVector[0] == nullptr)));
	if (ch){
		try{
			err = f[ch](BVector);
		}
		catch (char* &what){
			std::cout << what << std::endl;
			if (ch == 1)
				erase(BVector);
			return err;
		}
		
	}
	else
		err = 0;
	return err;
}

int create(Logic::BVector**v){
	int ch;
	erasefull(v);
	for (int i = 0; i < 3; i++){
		std::cout << "Create " << i + 1 << " vector:" << std::endl << "1. Default" << std::endl << "2. By amount of undefined elements" << std::endl << "3. With string" << std::endl;
		if (i)
			std::cout << "4. Copy" << std::endl;
		do{
			if (getNum(ch)){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (ch < 1 || ch > 4 || ((ch>3) && (i == 0))){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (ch < 1 || ch > 4 || ((ch > 3) && (i == 0)));
		switch (ch)
		{
		case 1:
			try{
				v[i] = new Logic::BVector;
			}
			catch (std::bad_alloc &ba){
				std::cout << "Failed to allocate memory" << ba.what() << std::endl;
				return 2;
			}
			break;
		case 2:
			int x;
			std::cout << "Amount of elements: ";
			std::cin >> x;
			try{
				v[i] = new Logic::BVector(x);
			}
			catch (std::bad_alloc &ba){
				std::cout << "Failed to allocate memory" << ba.what() << std::endl;
				return 2;
			}
			break;
		case 3:	
			char*s;
			std::cout << "Input string: ";
			try{
				s = getStr(std::cin);
			}
			catch (const char* what){
				std::cout << what<< std::endl;
				system("pause");
				return 2;
			}
			try{
				v[i] = new Logic::BVector(s);
			}
			catch (std::bad_alloc &ba){
				std::cout << "Failed to allocate memory" << ba.what() << std::endl;
				delete s;
				return 2;
			}
			delete s;
			break;
		case 4:
			int n;
			std::cout << "Input number of vector to copy: ";
			do{
				if (getNum(n)){
					std::cout << "Incorrect number" << std::endl;
					system("pause");
					return 1;
				}
				if (n < 1 || n > i){
					std::cout << "Incorrect number, input again" << std::endl;
				}
			} while (n < 1 || n > i);
			try{
				v[i] = new Logic::BVector(*(v[n-1]));
			}
			catch (std::bad_alloc &ba){
				std::cout << "Failed to allocate memory" << ba.what() << std::endl;
				return 2;
			}
			break;
		default:
			break;
		}
	}
	return 0;
}

int erase(Logic::BVector**v){
	erasefull(v);
	for (int i = 0; i < 3; i++)
		v[i] = nullptr;
	return 0;
}

int set(Logic::BVector**v){
	int n;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << "Input string: ";
	char* s;
	try{
		s = getStr(std::cin);
	}
	catch (const char* what){
		std::cout << what << std::endl;
		system("pause");
		return 2;
	}
	(*(v[n - 1])).SetVector(s);
	return 0;
}

int getVal(Logic::BVector**v){
	int n,num;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << "Value number: ";
	do{
		if (getNum(num)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (num < 1){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 );
	std::cout << (*(v[n - 1])).getVal(num - 1) << std::endl;
	return 0;
}

int getN(Logic::BVector**v){
	int n;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << (*(v[n-1])).getN() << std::endl;
	return 0;
}

int OR(Logic::BVector**v){
	int x[3];
	for (int i = 0; i < 3; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "z: ";
		if (i == 1)
			std::cout << "x: ";
		if (i == 2)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	(*(v[(x[0]-1)])) = (*(v[(x[1]-1)])) | (*(v[(x[2]-1)]));
	return 0;
}

int AND(Logic::BVector**v){
	int x[3];
	for (int i = 0; i < 3; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "z: ";
		if (i == 1)
			std::cout << "x: ";
		if (i == 2)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	(*(v[(x[0] - 1)])) = (*(v[(x[1] - 1)])) & (*(v[(x[2] - 1)]));
	return 0;
}

int EQAND(Logic::BVector**v){
	int x[2];
	for (int i = 0; i < 2; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "x: ";
		if (i == 1)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	(*(v[(x[0] - 1)])) &= (*(v[(x[1] - 1)]));
	return 0;
}

int NOT(Logic::BVector**v){
	int x[3];
	for (int i = 0; i < 3; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "z: ";
		if (i == 1)
			std::cout << "x: ";
		if (i == 2)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	(*(v[(x[0] - 1)])) = (*(v[(x[1] - 1)])) ^ (*(v[(x[2] - 1)]));
	return 0;
}

int DEF(Logic::BVector**v){
	int n;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << ((*(v[n - 1])).defined() ? "Vector is defined" : "Vector is not defined") << std::endl;
	return 0;
}

int output(Logic::BVector**v){
	int n;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << "Vector " << n << ": " << (*(v[n-1])) << std::endl;
	return 0;
}

int input(Logic::BVector**v){
	int n;
	std::cout << "Vector number: ";
	do{
		if (getNum(n)){
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (n < 1 || n > 3){
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (n < 1 || n > 3);
	std::cout << "Input string: ";
	std::cin >> (*(v[n - 1]));
	if(std::cin.fail()){
		std::cout << "Failed to input" << std::endl;
		system("pause");
		return 2;
	}
	return 0;
}

void erasefull(Logic::BVector**v){
	for (int i = 0; i < 3; i++)
		delete v[i];
}

int ISEQ(Logic::BVector**v){
	int x[2];
	for (int i = 0; i < 2; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "x: ";
		if (i == 1)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	std::cout << "Result: " << ((*(v[(x[0] - 1)])) ==(*(v[(x[1] - 1)])))<<std::endl;
	return 0;
}

int cut(Logic::BVector**v){
	int x[2];
	for (int i = 0; i < 2; i++){
		std::cout << "Input number of vector ";
		if (i == 0)
			std::cout << "x: ";
		if (i == 1)
			std::cout << "y: ";
		do{
			if (getNum(x[i])){
				std::cout << "Incorrect number" << std::endl;
				system("pause");
				return 1;
			}
			if (x[i] < 1 || x[i] > 3){
				std::cout << "Incorrect number, input again" << std::endl;
			}
		} while (x[i] < 1 || x[i] > 3);
	}
	(*(v[(x[0] - 1)])) = (*(v[(x[1] - 1)])).cut();
	return 0;
}