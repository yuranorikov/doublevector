#pragma once
#include <iostream>
#include <cstring>

char *getStr(std::istream &s);

namespace Logic{
	class BVector{
	private:
		int num;
		char* value;
		
	public:
		void erase();
		BVector();
		virtual ~BVector();
		BVector(const int &XN);
		BVector(const char* VAL);
		BVector(const BVector &V);
		BVector(BVector &&V);
		const int getN() const;
		const char getVal(int) const;
		friend BVector operator | (const Logic::BVector &S,const Logic::BVector &V)
		{
			int n = S.getN();
			char*C;
			if (n != V.getN())
				throw "Vectors have different length";
			try{
				C = new char[n + 1];
			}
			catch (std::bad_alloc &ba){
				throw"Failed to allocate memory";
			}
			C[n] = '\0';
			for (int i = 0; i < n; i++){
				if (S.getVal(i) == '1' || V.getVal(i) == '1')
					C[i] = '1';
				if (S.getVal(i) == '0' && V.getVal(i) == '0')
					C[i] = '0';
			}
			BVector res(C);
			delete C;
			return res;
		}
		friend const BVector operator & (const Logic::BVector &S, const Logic::BVector &V){
			int n = S.getN();
			char*C;
			if (n != V.getN())
				throw "Vectors have different length";
			try{
				C = new char[n + 1];
			}
			catch (std::bad_alloc &ba){
				throw"Failed to allocate memory";
			}
			C[n] = '\0';
			for (int i = 0; i < n; i++){
				if (S.getVal(i) == '1' && V.getVal(i) == '1')
					C[i] = '1';
				if (S.getVal(i) == '0' || V.getVal(i) == '0')
					C[i] = '0';
			}
			BVector res(C);
			delete C;
			return res;
		}
		const BVector cut() const;
		const BVector operator ^ (const Logic::BVector &V)const;
		const BVector& operator = (const Logic::BVector &V);
		const BVector& operator = (Logic::BVector &&V);
		const BVector& operator &= (const Logic::BVector &V);
		const bool operator == (const Logic::BVector &V) const;
		const BVector operator ~ () const;
		bool defined() const;
		friend std::ostream & operator <<(std::ostream &c, const BVector &r){
			int num = r.getN();
			for (int i = 0; i < num; i++){
				c << r.getVal(i);
				if (i < num - 1)
					c << ",";
			}
			return c;
		}
		friend std::istream & operator >>(std::istream &s, BVector &r)
		{
			char* val;
			val = getStr(s);
			if (s.bad()){
				s.clear();
				s.ignore(INT_MAX, '\n');
				delete val;
				s.setstate(std::istream::failbit);
				return s;
			}
			try{
				r.SetVector(val);
			}
			catch (char* err){
				delete val;
				s.setstate(std::istream::failbit);
				return s;
			}
			delete val;
			return s;
		}
		void SetVector(const char*VAL);
	};
}

