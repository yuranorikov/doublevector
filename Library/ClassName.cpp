#include "stdafx.h"
#include "ClassName.h"
#include <cmath>
#include <cstdio>
using namespace Logic;

BVector::BVector() :num(0),value(nullptr)
{
}

BVector::~BVector()
{
	delete value;
}

void BVector::erase()
{
	if (num == 0)
		return;
	num = 0;
	delete value;
	value = nullptr;
}

BVector::BVector(const int &XN) :num(0), value(nullptr)
{
	if (XN < 0)
		throw "Illegal value";
	if (XN == 0)
		return;
	num = XN;
	try{
		value = new char[num];
	}
	catch (std::bad_alloc &ba){
		num = 0;
		value=nullptr;
		throw "Failed to allocate memory";
	}
}

BVector::BVector(const BVector &V) :num(0), value(nullptr)
{
	*this = V;
}

BVector::BVector(const char* VAL) :num(0), value(nullptr)
{
	SetVector(VAL);
}

void BVector::SetVector(const char*VAL){
	if (num != 0)
		erase();
	for (num = 0; VAL[num]!='\0'; num++)
		if (VAL[num] != '0'&&VAL[num] != '1'&&VAL[num] != 'X'){
			num = 0;
			value = nullptr;
			throw "Illegal string";
		}
	try{
		value = new char[num];
	}
	catch (std::bad_alloc &ba){
		num = 0;
		value = nullptr;
		throw "Failed to allocate memory";
	}
	for (int i = 0; i < num; i++)
		value[i] = VAL[i];
}

const int BVector::getN() const
{
	return num;
}

const char BVector::getVal(int n) const
{
	if (n >= num)
		throw "Illegal value";
	return value[n];
}

const BVector BVector::operator ^ (const Logic::BVector &V) const{
	BVector res;
	char* C;
	if (this->num != V.getN())
		throw "Vectors have different length";
	if (num){
		try{
			C = new char[num+1];
			C[num] = '\0';
		}
		catch (std::bad_alloc &ba)
		{
			C = nullptr;
			throw "Failed to allocate memory";
		}
		for (int i = 0; i < num; i++){
			C[i] = getVal(i) == V.getVal(i) ? '0' : '1';
		}
		res.SetVector(C);
		delete C;

	}
	else
		res.SetVector(nullptr);
	return res;
}

const BVector BVector::cut() const {
	int b = -1, e = -1;
	BVector r;
	char* C;
	for (int i = 0; i< num; i++)
	{
		if (b == -1 && value[i] == '1')
			b = i;
		if (value[i] == '1')
			e = i + 1;
	}
	C = new char[e - b + 1];
	C[e - b] = '\0';
	for (int i = b; i < e; i++){
		C[i - b] = getVal(i);
	}
	r.SetVector(C);
	delete C;
	return r;
}
const BVector& BVector::operator = (const BVector &V)
{
	num = V.num;
	delete value;
	value = nullptr;
	if (num)
	try{
		value = new char[num];
	}
	catch (std::bad_alloc &ba)
	{
		num = 0;
		value = nullptr;
		throw "Failed to allocate memory";
	}
	for (int i = 0; i < num; i++){
		value[i] = V.getVal(i);
	}
	return *this;
}

const BVector& BVector::operator &= (const BVector &V)
{
	int min = this->num < V.num ? 0 : 1;
	int val = min ? V.num : num;
	char r;
	int i;
	for (i = 0; i <val; i++){
		if (this->value[i] == '1' && V.value[i] == '1')
			r = '1';
		if (this->value[i] == '0' || V.value[i] == '0')
			r = '0';
	}
	if (min == 0){
		char* b=new char[num];
		for (int j = 0; j < num; j++){
			b[j] = value[j];
		}
		delete value;
		this->value = new char[V.num];
		for (int j = 0; j < num; j++){
			value[j] = b[j];
		}
		this->num = V.num;
	}
	val = min ? num : V.num;
	for (; i < val; i++){
		if ((min ? V : *this).value[i] == '0')
			this->value[i] = '0';
	}
	return *this;
}

const bool BVector::operator == (const BVector &V) const
{
	if (this->num != V.num)
		return 0;
	for (int i = 0; i < this->num; i++){
		if (this->value[i] == '1' || V.value[i] == '0')
			return 0;
		if ((this->value[i] == '1'&&V.value[i] == '0') || (this->value[i] == '0'&&V.value[i] == '1'))
			return 0;
	}
	return 1;
}

const BVector BVector::operator ~ () const
{
	BVector res(this->num);
	for (int i = 0; i < this->num; i++){
		if (this->value[i] == '0')
			res.value[i] = '1';
		if (this->value[i] == '1')
			res.value[i] = '0';
	}
	return res;
}

bool BVector::defined() const
{
	for (int i = 0; i < num; i++)
		if (value[i] == '0')
		if (value[i] == '1')
			return false;
	return true;
}

char *getStr(std::istream &s)
{
	char buf[21];
	int n, len = 0, curLen;
	char *ptr = new char, *bptr;
	*ptr = '\0';
	s.get();
	do{
		s.get(buf, 21);
		if (s.bad()){
			delete ptr;
			ptr = nullptr;
			s.clear();
			s.ignore(INT_MAX, '\n');
			throw "Failed to get string";
		}
		if (s.good()){
			curLen = strlen(buf);
			len += curLen;
			bptr = ptr;
			try{
				ptr = new char[len + 1];
			}
			catch (std::bad_alloc &ba){
				s.clear();
				s.ignore(INT_MAX, '\n');
				delete bptr;
				throw "Failed to allocate memory for string";
			}
			*ptr = '\0';
			strcat_s(ptr, len + 1, bptr);
			strcat_s(ptr, len + 1, buf);
			delete bptr;
		}
		else
			s.get();
	} while (s.good());
	s.clear();
	s.ignore(INT_MAX, '\n');
	return ptr;
}

BVector::BVector(BVector &&V) :num(V.num), value(V.value)
{
	V.num = 0;
	V.value = nullptr;
}

const BVector& BVector::operator = (Logic::BVector &&V)
{
	int nb = num;
	num = V.num;
	V.num = nb;
	char*valb = value;
	value = V.value;
	V.value = valb;
	return *this;
}