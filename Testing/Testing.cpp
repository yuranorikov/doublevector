#include "stdafx.h"
#include "..\Library\ClassName.h"
#include "gtest\gtest.h"
#include <ctime>

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	int r = RUN_ALL_TESTS();
	system("pause");
	return r;
}

using Logic::BVector;

TEST(BVectorConstructionTesting, DefaultConstructor)
{
	BVector t;
	EXPECT_DOUBLE_EQ(0, t.getN());
	EXPECT_THROW(t.getVal(0),const char*);
}


TEST(BVectorConstructionTesting, ValueConstructor)
{
	ASSERT_NO_THROW(BVector("10"));
	BVector t1("10");
	EXPECT_DOUBLE_EQ(2, t1.getN());
	EXPECT_DOUBLE_EQ('1', t1.getVal(0));
	EXPECT_DOUBLE_EQ('0', t1.getVal(1));
	ASSERT_NO_THROW(BVector(""));
	BVector t2("");
	EXPECT_DOUBLE_EQ(0, t2.getN());
	EXPECT_THROW(t2.getVal(0), const char*);
	EXPECT_THROW(BVector("ERROR"),const char*);
}

TEST(BVectorConstructionTesting, CopyConstructor)
{
	BVector v("101");
	ASSERT_NO_THROW(BVector(v));
	BVector t(v);
	EXPECT_DOUBLE_EQ(3, t.getN());
	EXPECT_DOUBLE_EQ('1', t.getVal(0));
	EXPECT_DOUBLE_EQ('0', t.getVal(1));
	EXPECT_DOUBLE_EQ('1', t.getVal(2));
}

TEST(BVectorConstructionTesting, MoveConstructor)
{
	BVector &&v1("101");
	ASSERT_NO_THROW(BVector(std::move(v1)));
	BVector &&v2("101");
	BVector t(std::move(v2));
	EXPECT_DOUBLE_EQ(3, t.getN());
	EXPECT_DOUBLE_EQ('1', t.getVal(0));
	EXPECT_DOUBLE_EQ('0', t.getVal(1));
	EXPECT_DOUBLE_EQ('1', t.getVal(2));
}

TEST(BVectorFunctionTesting, SetVector)
{
	BVector t1;
	ASSERT_NO_THROW(t1.SetVector("101"));
	EXPECT_DOUBLE_EQ(3, t1.getN());
	EXPECT_DOUBLE_EQ('1', t1.getVal(0));
	EXPECT_DOUBLE_EQ('0', t1.getVal(1));
	EXPECT_DOUBLE_EQ('1', t1.getVal(2));
	BVector t2(3);
	ASSERT_NO_THROW(t2.SetVector("101"));
	EXPECT_DOUBLE_EQ(3, t2.getN());
	EXPECT_DOUBLE_EQ('1', t2.getVal(0));
	EXPECT_DOUBLE_EQ('0', t2.getVal(1));
	EXPECT_DOUBLE_EQ('1', t2.getVal(2));
}

TEST(BVectorConstructionTesting, EqualConstructor)
{
	BVector t1(3);
	ASSERT_NO_THROW(t1.SetVector("111"));
	EXPECT_DOUBLE_EQ(3, t1.getN());
	EXPECT_DOUBLE_EQ('1', t1.getVal(0));
	EXPECT_DOUBLE_EQ('1', t1.getVal(1));
	EXPECT_DOUBLE_EQ('1', t1.getVal(2));
	BVector t2(3);
	ASSERT_NO_THROW(t2.SetVector("101"));
	EXPECT_DOUBLE_EQ(3, t2.getN());
	EXPECT_DOUBLE_EQ('1', t2.getVal(0));
	EXPECT_DOUBLE_EQ('0', t2.getVal(1));
	EXPECT_DOUBLE_EQ('1', t2.getVal(2));
	ASSERT_NO_THROW(t1 = t2);
	EXPECT_DOUBLE_EQ(3, t1.getN());
	EXPECT_DOUBLE_EQ('1', t1.getVal(0));
	EXPECT_DOUBLE_EQ('0', t1.getVal(1));
	EXPECT_DOUBLE_EQ('1', t1.getVal(2));
}




TEST(BVectorFunctionTesting, Erase)
{
	srand(time(NULL));
	BVector t(rand()%100+1);
	ASSERT_NO_THROW(t.erase());
	EXPECT_DOUBLE_EQ(0, t.getN());
	EXPECT_THROW(t.getVal(0), const char*);
}

TEST(BVectorFunctionTesting, Defined)
{
	BVector t1("1101001011010100101");
	EXPECT_DOUBLE_EQ(1, t1.defined());
}

TEST(BVectorOperatorsTesting, EQUAL)
{
	BVector t11("1101001011010100101");
	BVector t12;
	ASSERT_NO_THROW(t12 = t11);
	EXPECT_DOUBLE_EQ(19, t12.getN());
	for (int i = 0; i < 19;i++)
		EXPECT_DOUBLE_EQ(1,t11.getVal(i)==t12.getVal(i));
	BVector t21("1001");
	BVector t22("0111");
	ASSERT_NO_THROW(t22 = t21);
	EXPECT_DOUBLE_EQ(4, t22.getN());
	for (int i = 0; i < 4; i++)
		EXPECT_DOUBLE_EQ(1, t21.getVal(i) == t22.getVal(i));
	BVector t31;
	BVector t32;
	ASSERT_NO_THROW(t32 = t31);
	EXPECT_DOUBLE_EQ(0, t32.getN());
	EXPECT_THROW(t32.getVal(0), const char*);
	BVector t41;
	BVector t42(5);
	ASSERT_NO_THROW(t42 = t41);
	EXPECT_DOUBLE_EQ(0, t42.getN());
	EXPECT_THROW(t42.getVal(0), const char*);
}

TEST(BVectorOperatorsTesting, MOVEEQUAL)
{
	BVector t11("1101001011010100101");
	BVector t12;
	ASSERT_NO_THROW(t12 = std::move(t11));
	EXPECT_DOUBLE_EQ(19, t12.getN());
	EXPECT_DOUBLE_EQ(0, t11.getN());
	BVector t21("1001");
	BVector t22("0111");
	ASSERT_NO_THROW(t22 = std::move(t21));
	EXPECT_DOUBLE_EQ(4, t22.getN());
	EXPECT_DOUBLE_EQ(4, t21.getN());
	EXPECT_DOUBLE_EQ('1', t22.getVal(0));
	EXPECT_DOUBLE_EQ('0', t22.getVal(1));
	EXPECT_DOUBLE_EQ('0', t22.getVal(2));
	EXPECT_DOUBLE_EQ('1', t22.getVal(3));
	EXPECT_DOUBLE_EQ('0', t21.getVal(0));
	EXPECT_DOUBLE_EQ('1', t21.getVal(1));
	EXPECT_DOUBLE_EQ('1', t21.getVal(2));
	EXPECT_DOUBLE_EQ('1', t21.getVal(3));
	BVector t31;
	BVector t32;
	ASSERT_NO_THROW(t32 = std::move(t31));
	EXPECT_DOUBLE_EQ(0, t32.getN());
	EXPECT_THROW(t32.getVal(0), const char*);
	EXPECT_DOUBLE_EQ(0, t31.getN());
	EXPECT_THROW(t31.getVal(0), const char*);
	BVector t41;
	BVector t42(5);
	ASSERT_NO_THROW(t42 = std::move(t41));
	EXPECT_DOUBLE_EQ(0, t42.getN());
	EXPECT_DOUBLE_EQ(5, t41.getN());
	EXPECT_THROW(t42.getVal(0), const char*);
}

TEST(BVectorOperatorsTesting, OR)
{
	BVector t11("1100101100");
	BVector t12("0101101101");
	BVector t13;
	ASSERT_NO_THROW(t13 = t11 | t12);
	EXPECT_DOUBLE_EQ(10, t13.getN());
	EXPECT_DOUBLE_EQ('1', t13.getVal(0));
	EXPECT_DOUBLE_EQ('1', t13.getVal(1));
	EXPECT_DOUBLE_EQ('0', t13.getVal(2));
	EXPECT_DOUBLE_EQ('1', t13.getVal(3));
	EXPECT_DOUBLE_EQ('1', t13.getVal(4));
	EXPECT_DOUBLE_EQ('0', t13.getVal(5));
	EXPECT_DOUBLE_EQ('1', t13.getVal(6));
	EXPECT_DOUBLE_EQ('1', t13.getVal(7));
	EXPECT_DOUBLE_EQ('0', t13.getVal(8));
	EXPECT_DOUBLE_EQ('1', t13.getVal(9));
	BVector t21("1010110");
	BVector t22("1001010");
	BVector t23;
	ASSERT_NO_THROW(t23 = t21 | t22);
	EXPECT_DOUBLE_EQ(7, t23.getN());
	EXPECT_DOUBLE_EQ('1', t23.getVal(0));
	EXPECT_DOUBLE_EQ('0', t23.getVal(1));
	EXPECT_DOUBLE_EQ('1', t23.getVal(2));
	EXPECT_DOUBLE_EQ('1', t23.getVal(3));
	EXPECT_DOUBLE_EQ('1', t23.getVal(4));
	EXPECT_DOUBLE_EQ('1', t23.getVal(5));
	EXPECT_DOUBLE_EQ('0', t23.getVal(6));
	BVector t31(5);
	BVector t32(7);
	EXPECT_THROW(t31|t32,const char*);
	BVector t41;
	BVector t42;
	BVector t43;
	ASSERT_NO_THROW(t43 = t41 | t42);
	EXPECT_DOUBLE_EQ(0, t43.getN());
	EXPECT_THROW(t43.getVal(0), const char*);
}

TEST(BVectorOperatorsTesting, AND)
{
	BVector t11("1100101100");
	BVector t12("0101101101");
	BVector t13;
	ASSERT_NO_THROW(t13 = t11 & t12);
	EXPECT_DOUBLE_EQ(10, t13.getN());
	EXPECT_DOUBLE_EQ('0', t13.getVal(0));
	EXPECT_DOUBLE_EQ('1', t13.getVal(1));
	EXPECT_DOUBLE_EQ('0', t13.getVal(2));
	EXPECT_DOUBLE_EQ('0', t13.getVal(3));
	EXPECT_DOUBLE_EQ('1', t13.getVal(4));
	EXPECT_DOUBLE_EQ('0', t13.getVal(5));
	EXPECT_DOUBLE_EQ('1', t13.getVal(6));
	EXPECT_DOUBLE_EQ('1', t13.getVal(7));
	EXPECT_DOUBLE_EQ('0', t13.getVal(8));
	EXPECT_DOUBLE_EQ('0', t13.getVal(9));
	BVector t21("1010110");
	BVector t22("1001010");
	BVector t23;
	ASSERT_NO_THROW(t23 = t21 & t22);
	EXPECT_DOUBLE_EQ(7, t23.getN());
	EXPECT_DOUBLE_EQ('1', t23.getVal(0));
	EXPECT_DOUBLE_EQ('0', t23.getVal(1));
	EXPECT_DOUBLE_EQ('0', t23.getVal(2));
	EXPECT_DOUBLE_EQ('0', t23.getVal(3));
	EXPECT_DOUBLE_EQ('0', t23.getVal(4));
	EXPECT_DOUBLE_EQ('1', t23.getVal(5));
	EXPECT_DOUBLE_EQ('0', t23.getVal(6));
	BVector t31(5);
	BVector t32(7);
	EXPECT_THROW(t31 & t32, const char*);
	BVector t41;
	BVector t42;
	BVector t43;
	ASSERT_NO_THROW(t43 = t41 & t42);
	EXPECT_DOUBLE_EQ(0, t43.getN());
	EXPECT_THROW(t43.getVal(0), const char*);
}

TEST(BVectorOperatorsTesting, ISEQ)
{
	BVector t11("1100101100");
	BVector t12(t11);
	EXPECT_DOUBLE_EQ(0, t11 == t12);
	EXPECT_DOUBLE_EQ(0, t12 == t11);
	BVector t21("110010110");
	BVector t22("110010110");
	EXPECT_DOUBLE_EQ(0, t21 == t22);
	EXPECT_DOUBLE_EQ(0, t22 == t21);
	BVector t31("110010110");
	BVector t32("110010110");
	EXPECT_DOUBLE_EQ(0, t31 == t32);
	EXPECT_DOUBLE_EQ(0, t32 == t31);
	BVector t41("110110110");
	BVector t42("111010100");
	EXPECT_DOUBLE_EQ(0, t41 == t42);
	EXPECT_DOUBLE_EQ(0, t42 == t41);
	BVector t51("110010110");
	BVector t52("110010110");
	EXPECT_DOUBLE_EQ(0, t51 == t52);
	EXPECT_DOUBLE_EQ(0, t52 == t51);

}

/*TEST(BVectorOperatorsTesting, ANDEQ)
{
	BVector t11("1100101100");
	BVector t12("0101101101");
	BVector t13;
	t13 = t11 & t12;
	ASSERT_NO_THROW(t11 &= t12);
	EXPECT_DOUBLE_EQ(10, t11.getN());
	EXPECT_DOUBLE_EQ(1, t11==t13);
	BVector t21("1011110");
	BVector t22("1001010");
	ASSERT_NO_THROW(t21 &= t22);
	EXPECT_DOUBLE_EQ('1', t21.getVal(0));
	EXPECT_DOUBLE_EQ('0', t21.getVal(1));
	EXPECT_DOUBLE_EQ('1', t21.getVal(2));
	EXPECT_DOUBLE_EQ('0', t21.getVal(3));
	EXPECT_DOUBLE_EQ('1', t21.getVal(4));
	EXPECT_DOUBLE_EQ('0', t21.getVal(5));
	EXPECT_DOUBLE_EQ('0', t21.getVal(6));
	BVector t31(5);
	BVector t32(7);
	ASSERT_NO_THROW(t31 &= t32);
	BVector t41;
	BVector t42;
	ASSERT_NO_THROW(t41 &= t42);
	EXPECT_DOUBLE_EQ(0, t41.getN());
	EXPECT_THROW(t41.getVal(0), const char*);
}*/

TEST(BVectorOperatorsTesting, NOT){
	BVector t21("1101");
	BVector t22("1001");
	BVector t23("1001");
	ASSERT_NO_THROW(t23=t22 ^ t21);
	EXPECT_DOUBLE_EQ(4, t23.getN());
	EXPECT_DOUBLE_EQ('0', t23.getVal(0));
	EXPECT_DOUBLE_EQ('1', t23.getVal(1));
	EXPECT_DOUBLE_EQ('0', t23.getVal(2));
	EXPECT_DOUBLE_EQ('0', t23.getVal(3));
}